export const ENV={
//    serialPort:'/dev/ttyS3',   // linux
    serialPort:'\\\\.\\COM5', // windows
    baudRate:9600,
    parity:'none',   // valid values 'none'|'even'|'odd'
    stopBits:1,
    timeoutms:1500,
    firmware:{
        outdir:'C:\\node\\output\\nonwifi\\',
        inputfile:'C:/node/firmware/nonwifi/solar-relay-firmware-33.production.hex',
        version:33,
        version_address : 0x1498d,
        serialnumber_address : 0x74c,
//        serialnumber_address : 0xda40,
        default_serialnumber : 61003,
        ipecmd:'"C:\\Program Files\\Microchip\\MPLABX\\v5.45\\mplab_platform\\mplab_ipe\\ipecmd"'
    },
    bands:{
        v:0.01,
        i:0.05,
        f:0.01,
        
    }
}