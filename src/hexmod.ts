import * as fs from 'fs';
import * as readline from 'readline';


export interface IHexLine {
    lineNum:number;
    hexDataSize:string;
    decimalDataSize:number;
    hexAddress:string;
    decimalAddress:number;
    rType:string;
    data:string;
    checksum:string;
};


export class Hexmod{

	public lines:IHexLine[] = [];
	
	private baseaddress = 0;

    constructor(){

        
    }


    hexToDec(hex:string):number{ return parseInt(hex,16); }
    DecToHex(dec:number):string{ return dec.toString(16).padStart(2,'0'); }


	private sumbytes(str:string):number{
		let sum = 0;

        const arr = str.match(/.{1,2}/g);

        if(typeof arr === 'undefined' || arr === null)
          return 0;

//		$arr = explode(",",trim(chunk_split($str,2,","),","));

		arr.forEach((val)=>{sum += this.hexToDec(val);});
			
		return sum;
	}


    findAddress(addr:number, scale:number = 1):number{


        for(let i=0; i< this.lines.length; i++){
            const line = this.lines[i];
            const dataSize = line.decimalDataSize/scale;

            if(line.decimalAddress <= addr && addr < line.decimalAddress + dataSize)
              return i;

        }

        return -1;
    }


    private dataOffset(base:number, required:number, scale:number){

        return 2*(required - base)*scale;
    }

    getDataAt(addr:number, bytes:number, scale:number = 1):string{

          // we get the data one byte at at time so if the data crosses
          // two lines it is no problem

          let result = '';

          for(let i=0; i<bytes; i++){

            const myAddr = (addr+i)*scale;
            const line_idx  = this.findAddress(myAddr,scale);

            if(line_idx == -1)
              return '';


            const line = this.lines[line_idx];

            const offset = this.dataOffset(line.decimalAddress,myAddr,scale);

            result = result + line.data.substr(offset,2);

          }

          return result;

    }


    findDataAddress(intVal:number, scale=1):string[]{

        const search = this.DecToHex(intVal&0x00ff)+this.DecToHex((intVal>>8)&0x00ff);

        const result:string[] = []
        for(let idx=0; idx<this.lines.length; idx++){
            let line = this.lines[idx];

            const offset  = line.data.indexOf(search);

            if(offset>=0){

                // found a match here.
                const addr = offset/(2*scale)+line.decimalAddress;
                result.push(this.DecToHex(addr)); 
 //               console.log(line);
            }

        }

        return result;

    }

    setIntAt(addr:number,intVal:number,scale:number = 1){
        this.setByteAt(addr,this.DecToHex(intVal&0x00ff),scale);
        this.setByteAt(addr+1,this.DecToHex((intVal>>8)&0x00ff),scale);    
    }


    getIntAt(addr:number,scale:number = 1):number{

//        console.log('hex1',this.getDataAt(addr+1,1));
//        console.log('hex2',this.getDataAt(addr,1));

        let hex = this.getDataAt(addr+1,1);
        hex = hex + this.getDataAt(addr,1);

//        console.log('getIntAt:',this.DecToHex(addr),hex);

        return parseInt(hex,16);

    }



    setByteAt(addr:number, byte:string, scale:number = 1){

        // we get the data one byte at at time so if the data crosses
        // two lines it is no problem


        const myAddr = addr*scale;


        const line_idx  = this.findAddress(myAddr,scale);

        if(line_idx == -1)
           return;

        const line = this.lines[line_idx];

        const offset = this.dataOffset(line.decimalAddress,myAddr,scale);
   //      console.log('before',line);
         line.data = this.replaceAt(line.data,offset,byte);
   //      console.log('after ',line);



  }    


    private replaceAt(str:string, index:number, replacement:string):string{
        return str.substr(0, index) + replacement + str.substr(index + replacement.length);
    }

    private processLine(idx:number,line:string){

        const hexAddress = line.substr(3,4);
        const dataSize = line.substr(1,2);
        const decDataSize = this.hexToDec(dataSize);
        const rtype = line.substr(7,2);
        const data = line.substr(9,decDataSize*2);


        if(rtype == '02'){
            this.baseaddress = parseInt(data,16) <<4;
        }else if(rtype == '04'){
            this.baseaddress = parseInt(data,16) <<16;
        }


        this.lines.push({
             lineNum:idx
            ,hexDataSize : dataSize
            ,decimalDataSize: decDataSize
            ,hexAddress : hexAddress
            ,decimalAddress : this.baseaddress+this.hexToDec(hexAddress)
            ,rType : line.substr(7,2)
            ,data : data
            ,checksum : line.substr(decDataSize+10,2)
        });

        

    }


    async writeToFile(filename:string){

        if(fs.existsSync(filename)){
            fs.unlinkSync(filename);
        }

        
         for(let idx=0; idx<this.lines.length; idx++){
            let line = this.lines[idx];

            let checksum = this.sumbytes(line.hexDataSize);
            checksum    += this.sumbytes(line.hexAddress);
            checksum    += this.sumbytes(line.rType);
            checksum    += this.sumbytes(line.data);

            checksum = -checksum;

            let str_check = this.DecToHex(checksum&255).padStart(2,'0');
            line.checksum = str_check;

            fs.appendFileSync(filename,
                ':'+line.hexDataSize+line.hexAddress+line.rType+line.data+line.checksum+'\r\n'
                );
        }


    }

    async load(filename:string):Promise<void>{



          if(!fs.existsSync(filename)){
              console.log('Error '+filename+' does not exist');
              process.exit(1);
          }


        const fileStream = fs.createReadStream(filename);
        const rl = readline.createInterface({
            input: fileStream,
            crlfDelay: Infinity
          }); 

        let idx = 0;
        for await (const line of rl) {
            idx++;
            this.processLine(idx,line);
                

        }
        fileStream.close();    

    }

}