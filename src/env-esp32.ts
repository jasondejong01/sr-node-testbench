export const ENV={
//    serialPort:'/dev/ttyS3',   // linux
    serialPort:'\\\\.\\COM5', // windows
    baudRate:9600,
    parity:'none',   // valid values 'none'|'even'|'odd'
    stopBits:1,
    timeoutms:1500,
    mc_firmware:{
        outdir:'C:\\node\\output\\esp32\\',
        inputfile:'C:\\esp-relay\\production-build\\solar-relay-esp32.production.unified.hex',
        ipecmd:'"C:\\Program Files\\Microchip\\MPLABX\\v5.45\\mplab_platform\\mplab_ipe\\ipecmd"'
    },
    esp32_firmware:{
         ver_file:'C:\\esp-relay\\production-build\\version.json',
         mfg_loader:'C:\\esp-relay\\production-build\\mfgload.bat'
    },
    bands:{
        v:0.01,
        i:0.05,
        f:0.01,
        
    }
}