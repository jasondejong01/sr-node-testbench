import { ModbusDevice } from "./modbus-device";
import ModbusRTU from 'modbus-serial';
import {ENV} from './env-esp32';
import fs from 'fs';
import { sleep, sleepUntil } from "./sleep";
import { Hexmod } from "./hexmod";
import moment from 'moment'
import * as MODBUS_TYPES from './modbus-typedefs'

var args = process.argv.slice(2);



if(args.length != 1 && args.length != 3){
    console.log('Usage: ts-node run <serialnumber> <skipprogram ESP32> <skipprogram PIC24>');
    process.exit();
}


const SERIALNUMBER:number = +args[0];
let SKIPPROGRAM_ESP      = 0;
let SKIPPROGRAM_PIC24      = 0;

if(args.length == 3){
    SKIPPROGRAM_ESP = +args[1];
     SKIPPROGRAM_PIC24 = +args[2];

}



write(SERIALNUMBER,'***');
write(SERIALNUMBER,'***');
write(SERIALNUMBER,'***');
write(SERIALNUMBER,'*******************************************************************');
write(SERIALNUMBER,'*                                                                 *');
write(SERIALNUMBER,'*                                                                 *');
write(SERIALNUMBER,'*                  SOLAR RELAY TEST BENCH for ESP32               *');
write(SERIALNUMBER,'*                                                                 *');
write(SERIALNUMBER,'*                                                                 *');
write(SERIALNUMBER,'*******************************************************************');
write(SERIALNUMBER,'');
write(SERIALNUMBER,'PIC24 Firmware File: ',ENV.mc_firmware.inputfile);

if(!fs.existsSync(ENV.mc_firmware.inputfile)){
    write(SERIALNUMBER,'ERROR: PIC24 Firmware file not found: ',ENV.mc_firmware.inputfile);
    process.exit();
}



write(SERIALNUMBER,'connecting to test bench');



const client = new ModbusRTU();
client.setTimeout (ENV.timeoutms);


        // establish the serial connection. once the connection is established a call to run()
        // will be made
        client.connectRTUBuffered(ENV.serialPort, { baudRate: ENV.baudRate
                                                ,parity:ENV.parity as 'none'|'even'|'odd'
                                                , stopBits:ENV.stopBits })                                   
                .then(async()=>{

                        write(SERIALNUMBER,'connected!');
                        await run();                                                        
                })
                .catch((error)=>{

                    console.log(error);
                    process.exit();
            

                });



function beep(){
    console.log('\x07');
}

function write(serialNo:number,...args:any[]):void{

    console.log(...args);

    let msg = moment().format('DD-MM-YYYY HH:mm')+' - ';
    for (let i= 0; i< args.length; i++)
       msg += args[i];

    fs.appendFileSync(ENV.mc_firmware.outdir+serialNo+'.log',msg+'\r\n');
}


function error(serialNo:number,...args:any[]):void{
    write(serialNo,'ERROR',...args);
    beep();
    process.exit();
}




async function pauseme():Promise<void>{

    return new Promise((resolve)=>{


        console.log('Press ENTER to continue.');
        process.stdin.once('data', function () {
          resolve();
        });
    

    });


}


async function ipecmd(filename:string):Promise<boolean>{

    return new Promise((resolve)=>{
            const exec = require('child_process').exec;


            let c = '"C:\\Program Files\\Microchip\\MPLABX\\v5.45\\mplab_platform\\mplab_ipe\\ipecmd"';
            c += ' -P24FJ128GA306';  // device family
            c += ' -TPICD4';         // specify which programmer to use
            c += ' -E';              // Erase the device
            c += ' -M';              // program device
            c += ' -OL';             // release from reset
            c += ' -F'+'"'+filename+'"';

            write(SERIALNUMBER,c);
            exec(c,(error:any, stdout:any, stderr:any) => {

                    write(SERIALNUMBER,'stdout:',stdout);

                    if(typeof stderr !== 'undefined' && stderr !== null)
                      write(SERIALNUMBER,'stderr:',stderr);
                    
                    if(typeof stderr !== 'undefined' && stderr !== null)
                      write(SERIALNUMBER,'error :',stderr);

                    if(error === null)
                        resolve(true);
                    else
                        resolve(false);

            });

        });       
}


async function mfgloader():Promise<boolean>{

  return new Promise((resolve)=>{
    const exec = require('child_process').exec;

    let c = '"'+ENV.esp32_firmware.mfg_loader+'"';
    write(SERIALNUMBER,c);
    exec(c,(error:any, stdout:any, stderr:any) => {

        write(SERIALNUMBER,'stdout:',stdout);

        if(typeof stderr !== 'undefined' && stderr !== null)
          write(SERIALNUMBER,'stderr:',stderr);
        
        if(typeof stderr !== 'undefined' && stderr !== null)
          write(SERIALNUMBER,'error :',stderr);

        if(error === null)
            resolve(true);
        else
            resolve(false);

  });

}); 

}

async function loadFirmware(serialNumber:number):Promise<void>{

    if(SKIPPROGRAM_ESP == 0){
            write(SERIALNUMBER,'Loading ESP32 firmware');

            var fs = require('fs').promises;
        
            var data = await fs.readFile(ENV.esp32_firmware.ver_file);
            var verObject = JSON.parse(data);

            write(SERIALNUMBER,'Module Type: ',verObject.module_type, 'fwVer:',verObject.fw_version);

            // now we need to write out the command file to use to build the sysident partition image
            // that will subsequently we loaded into the espressif chip

            var str = 'module_type,fw_version,serial_number\r\n';
                str+=  verObject.module_type +','+verObject.fw_version+','+serialNumber;



            await fs.writeFile('esp32_sysident.csv',str);

            const esp_result = await mfgloader();
            write(SERIALNUMBER,'esp32_result:',esp_result);

            if(!esp_result){
                error(SERIALNUMBER,'ERROR: Programming Finished with error');
            }

            write(SERIALNUMBER,'Finished with Espressif');
    }

    if(SKIPPROGRAM_PIC24 == 0){
            write(SERIALNUMBER,'Loading PIC24 Firmware');
            const programresult = await ipecmd(ENV.mc_firmware.inputfile);
            write(SERIALNUMBER,'');

            if(!programresult){
                error(SERIALNUMBER,'ERROR: Programming Finished with error');
            }
    }
    

}


function flt(n:number):string{

    return n.toFixed(2).padStart(8,' ');

}



function check1( label:string, meas:number,ref:number,band:number):number{

    let result = 1;

    if(meas <= ref+(ref*band) && meas >= ref-(ref*band)){
        write(SERIALNUMBER,label.padEnd(20,' ')+': PASS');
        result = 0;
     }else{
        write(SERIALNUMBER,label.padEnd(20,' ')+': FAIL','got',meas.toFixed(4),'expected',ref.toFixed(4),'+-',(ref*band).toFixed(4));
     }  


     return result;

}



function analyze(dc_bench:MODBUS_TYPES.IREADRESULT[]
                ,ac_bench:MODBUS_TYPES.IREADRESULT[]
                ,dc_dut  :MODBUS_TYPES.IREADRESULT[]
                ,ac_dut  :MODBUS_TYPES.IREADRESULT[]){

     let result = 0;


     result += check1('DUT1 DC Voltage',ModbusDevice.getResult(dc_dut,'v_r'),0,0);
     result += check1('DUT1 AC Voltage',ModbusDevice.getResult(ac_dut,'v_r'),ModbusDevice.getResult(ac_bench,'vr'),ENV.bands.v);
     result += check1('DUT1 DC Ir',ModbusDevice.getResult(dc_dut,'i_r'),0.1,1);
     result += check1('DUT1 AC Ir',ModbusDevice.getResult(ac_dut,'i_r'),ModbusDevice.getResult(ac_bench,'i_r'),ENV.bands.i);
     result += check1('DUT1 DC Is',ModbusDevice.getResult(dc_dut,'i_s'),0.1,1);
     result += check1('DUT1 AC Is',ModbusDevice.getResult(ac_dut,'i_s'),ModbusDevice.getResult(ac_bench,'i_s'),ENV.bands.i);



     if(result > 0){

         error(SERIALNUMBER,'Analysis show ERROR');

     }


}


async function readNoResponse(device:ModbusDevice, client:ModbusRTU, reg:string){

    try{await device.readByName(client,reg);}catch(e){}
}




async function run(){

    const bench = new ModbusDevice('test-bench',2);
    const dut = new ModbusDevice('solar-relay-esp',1);
    
    await bench.init();
    await dut.init();
    

    if(!SKIPPROGRAM_ESP || !SKIPPROGRAM_PIC24){
        write(SERIALNUMBER,'loading firmware');
        await loadFirmware(+SERIALNUMBER);

        await sleep(15000);
        // program the device
    }else{
        write(SERIALNUMBER,'Waiting until the bootloader has finished');
        await pauseme();
    }

    write(SERIALNUMBER,'Retrieving Measurements from TEST BOARD');
    const dc_bench_measures = await bench.read(client,108,18);


    await sleep(2000);



    write(SERIALNUMBER,'MEASUREMENTS - D C- TESTBOARD:');
    write(SERIALNUMBER,'                             AC Voltage: ',flt(ModbusDevice.getResult(dc_bench_measures,'vr')||0));
    write(SERIALNUMBER,'                                CTR rms: ',flt(ModbusDevice.getResult(dc_bench_measures,'i_r')||0));
    write(SERIALNUMBER,'                                CTS rms: ',flt(ModbusDevice.getResult(dc_bench_measures,'i_s')||0));
    write(SERIALNUMBER,'                      DUT1 TRIAC Output: ',flt(ModbusDevice.getResult(dc_bench_measures,'dut1_vrms')||0));
    write(SERIALNUMBER,'                      DUT2 TRIAC Ouptut: ',flt(ModbusDevice.getResult(dc_bench_measures,'dut2_vrms')||0));
    write(SERIALNUMBER,'   5v SUpply Current No DUT Powered(mA): ',flt(ModbusDevice.getResult(dc_bench_measures,'5v_noload_i')||0));
    write(SERIALNUMBER,'   5V Supply Current Avg(mA)           : ',flt(ModbusDevice.getResult(dc_bench_measures,'5v_i_avg')||0));
    write(SERIALNUMBER,'   5V Supply Current Max(mA)           : ',flt(ModbusDevice.getResult(dc_bench_measures,'5v_i_max')||0));
    write(SERIALNUMBER,'   5V Supply Current Min(mA)           : ',flt(ModbusDevice.getResult(dc_bench_measures,'5v_i_min')||0));
    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'');

    
    await sleep(1000);
    

    write(SERIALNUMBER,'Reading measurements from DUT 1');   
    const dc_dut_measures = await dut.read(client,2,28);
  

    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'MEASURMENTS - DC - DUT1');
    write(SERIALNUMBER,'                                   Vrms:', flt(ModbusDevice.getResult(dc_dut_measures,'v_r')||0));
    write(SERIALNUMBER,'                             Imains-rms:', flt(ModbusDevice.getResult(dc_dut_measures,'i_r')||0));
    write(SERIALNUMBER,'                             Isolar-rms:', flt(ModbusDevice.getResult(dc_dut_measures,'i_s')||0));
    write(SERIALNUMBER,'                             Pmains-rms:', flt(ModbusDevice.getResult(dc_dut_measures,'pwr_r')||0));
    write(SERIALNUMBER,'                             Psolar-rms:', flt(ModbusDevice.getResult(dc_dut_measures,'pwr_s')||0));
    write(SERIALNUMBER,'                             Freq mains:', flt(ModbusDevice.getResult(dc_dut_measures,'freq_r')||0));
    write(SERIALNUMBER,'                             Freq solar:', flt(ModbusDevice.getResult(dc_dut_measures,'freq_s')||0));
    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'');



    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'Next step is to apply HV');
    write(SERIALNUMBER,'');

    await sleep(300);

    write(SERIALNUMBER,'** WARNING: HV Being applied **');

    await readNoResponse(bench,client,'dut1_hv_power');
    await sleep(500);

    if(!await sleepUntil(  async ()=>{return (await bench.readByName(client,'dut1_hv_applied')) == 1},15000)){
        error(SERIALNUMBER,'Failed to get a Apply HV power to DUT1 - Test bnech is not working');
     }


    await sleep(300);

    write(SERIALNUMBER,'HV has been applied');
    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'Wait until bootlader finishes...');
    await pauseme();


   write(SERIALNUMBER,'Turning DUT1 Output on');
   await dut.writeSingleRegisterByName(client,'duty',1);

   await sleep(1000);


    write(SERIALNUMBER,'Reading measurements from DUT 1');   
    const ac_dut_measures = await dut.read(client,2,26);

    await sleep(300);

    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'MEASUREMENTS - AC -DUT1');
    write(SERIALNUMBER,'                                   Vrms:', flt(ModbusDevice.getResult(ac_dut_measures,'v_r')||0));
    write(SERIALNUMBER,'                             Imains-rms:', flt(ModbusDevice.getResult(ac_dut_measures,'i_r')||0));
    write(SERIALNUMBER,'                             Isolar-rms:', flt(ModbusDevice.getResult(ac_dut_measures,'i_s')||0));
    write(SERIALNUMBER,'                             Pmains-rms:', flt(ModbusDevice.getResult(ac_dut_measures,'pwr_r')||0));
    write(SERIALNUMBER,'                             Psolar-rms:', flt(ModbusDevice.getResult(ac_dut_measures,'pwr_s')||0));
    write(SERIALNUMBER,'                             Freq mains:', flt(ModbusDevice.getResult(ac_dut_measures,'freq_r')||0));
    write(SERIALNUMBER,'                             Freq solar:', flt(ModbusDevice.getResult(ac_dut_measures,'freq_s')||0));
    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'');




    console.log('Retrieving Measurements from TEST BOARD');
    const ac_bench_measures = await bench.read(client,108,18);

    await sleep(300);


    write(SERIALNUMBER,'MEASUREMENTS - AC - TESTBOARD:');
    write(SERIALNUMBER,'                             AC Voltage: ',flt(ModbusDevice.getResult(ac_bench_measures,'vr')||0));
    write(SERIALNUMBER,'                                CTR rms: ',flt(ModbusDevice.getResult(ac_bench_measures,'i_r')||0));
    write(SERIALNUMBER,'                                CTS rms: ',flt(ModbusDevice.getResult(ac_bench_measures,'i_s')||0));
    write(SERIALNUMBER,'                      DUT1 TRIAC Output: ',flt(ModbusDevice.getResult(ac_bench_measures,'dut1_vrms')||0));
    write(SERIALNUMBER,'                      DUT2 TRIAC Ouptut: ',flt(ModbusDevice.getResult(ac_bench_measures,'dut2_vrms')||0));
    write(SERIALNUMBER,'   5v SUpply Current No DUT Powered(mA): ',flt(ModbusDevice.getResult(ac_bench_measures,'5v_noload_i')||0));
    write(SERIALNUMBER,'   5V Supply Current Avg(mA)           : ',flt(ModbusDevice.getResult(ac_bench_measures,'5v_i_avg')||0));
    write(SERIALNUMBER,'   5V Supply Current Max(mA)           : ',flt(ModbusDevice.getResult(ac_bench_measures,'5v_i_max')||0));
    write(SERIALNUMBER,'   5V Supply Current Min(mA)           : ',flt(ModbusDevice.getResult(ac_bench_measures,'5v_i_min')||0));
    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'');

   await sleep(300);

   let dutOutputVoltage = ModbusDevice.getResult(ac_bench_measures,'dut1_vrms')||0;

   if(dutOutputVoltage < 200){
        error(SERIALNUMBER,'DUT Output did not turn contactor on. Measured voltage was only',dutOutputVoltage);
   }

   write(SERIALNUMBER,'Shutting down Output of DUT1');

   await sleep(300);
   await dut.writeSingleRegisterByName(client,'duty',0);

   await sleep(3000); 
   dutOutputVoltage = await bench.readByName(client,'dut1_vrms');

   if(dutOutputVoltage > 10){
       error(SERIALNUMBER,'DUT Output did not turn contactor off. Measured voltage is still',dutOutputVoltage);
   }

   await sleep(300);

   
    analyze(dc_bench_measures,ac_bench_measures,dc_dut_measures,ac_dut_measures);



    await sleep(300);
    await dut.writeSingleRegisterByName(client,'duty',1);

    await pauseme();

    await sleep(300);
    await dut.writeSingleRegisterByName(client,'duty',0);

    write(SERIALNUMBER,"*** TESTING COMPLETE ***");

    process.exit();


} 

