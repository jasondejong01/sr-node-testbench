import { ModbusDevice } from "./modbus-device";
import ModbusRTU from 'modbus-serial';
import {ENV} from './env';
import fs from 'fs';
import { sleep, sleepUntil } from "./sleep";
import { Hexmod } from "./hexmod";
import moment from 'moment'
import * as MODBUS_TYPES from './modbus-typedefs'

var args = process.argv.slice(2);



if(args.length != 1 && args.length != 2){
    console.log('Usage: ts-node run <serialnumber> <skipprogram>');
    process.exit();
}


const SERIALNUMBER:number = +args[0];
let SKIPPROGRAM      = 0;

if(args.length == 2)
    SKIPPROGRAM = +args[1];






const client = new ModbusRTU();
client.setTimeout (ENV.timeoutms);


        // establish the serial connection. once the connection is established a call to run()
        // will be made
        client.connectRTUBuffered(ENV.serialPort, { baudRate: ENV.baudRate
                                                ,parity:ENV.parity as 'none'|'even'|'odd'
                                                , stopBits:ENV.stopBits })                                   
                .then(async()=>{

                        console.log('connected!');
                        await run();                                                        
                })
                .catch((error)=>{

                    console.log(error);
                    process.exit();
            

                });



function beep(){
    console.log('\x07');
}






async function run(){

beep();



    process.exit();


} 

