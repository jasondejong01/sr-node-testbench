import fs from 'fs'
import ModbusRTU from 'modbus-serial';
import * as MODBUS_TYPES from './modbus-typedefs';
import {sleep}from './sleep'



interface IBufferResult{
    data:number[];
}

interface IWriteResult{
    address:number;
    value:number;
}


export class ModbusDevice{

    public mapFile:string;
    public map:MODBUS_TYPES.IMODBUSMAP;

    constructor(public deviceName:string
              , public deviceId:number){


        this.mapFile = MODBUS_TYPES.MAP_LOCATION+'/'+this.deviceName;
        this.map = {registers:[], requests:[]};


    }


    async init():Promise<void>{

           try{
              const f = this.mapFile;
              this.map = new (await import(f)).default();

           }catch(err){
              console.log('ERROR: Device mapping file ',this.mapFile,'cannot beloaded');
              console.log(err);
              process.exit();
           }

    }


    findRegisterByAddr(addr:number):MODBUS_TYPES.IREGISTER|null{


          for(let idx=0; idx<this.map.registers.length; idx++){

                if(this.map.registers[idx].address == addr)
                  return this.map.registers[idx];
          }

          return null;

    }


    findRegisterByName(name:string):MODBUS_TYPES.IREGISTER|null{


        for(let idx=0; idx<this.map.registers.length; idx++){

              if(this.map.registers[idx].name === name)
                return this.map.registers[idx];
        }

        return null;

  }    

    toFloat32(byte1:number, byte2:number, byte3:number, byte4:number):number{
        return Buffer.from([ byte1, byte2, byte3, byte4 ]).readFloatBE(0);
    }

    swap16(val:number):number{
        return ((val & 0xFF) << 8)
           | ((val >> 8) & 0xFF);    
    }    

    formatResult(register:MODBUS_TYPES.IREGISTER,registerIdx:number,buff:IBufferResult):number{


           let result:number = 0;

           switch(register.format){

                case MODBUS_TYPES.FORMAT.int16:
                   result =  buff.data[registerIdx];
                   break;

                case MODBUS_TYPES.FORMAT.int32:
                    result =  buff.data[registerIdx]<<16 + buff.data[registerIdx+1];
                    break;

                case MODBUS_TYPES.FORMAT.float32:
                        result = this.toFloat32(
                                this.swap16(buff.data[registerIdx])&0x00FF
                                ,(this.swap16(buff.data[registerIdx])>>8)&0x00FF
                                ,this.swap16(buff.data[registerIdx+1])&0x00FF
                                ,(this.swap16(buff.data[registerIdx+1])>>8)&0x00FF
                                );
                        break;
                        

           }

           result = result*register.scale;

           return result;



    }


    private processResult(startAddress:number,registerCount:number, buff:IBufferResult):MODBUS_TYPES.IREADRESULT[]{

//        console.log(buff);

        const result:MODBUS_TYPES.IREADRESULT[] = [];

        for(let idx = 0; idx<registerCount; idx++){

            const address = startAddress+idx;


            const register = this.findRegisterByAddr(address);

            if(register === null)
              continue;
    
            // if we get here we have found this register
            result.push({
                name: register.name,
                address: address,
                value: this.formatResult(register,idx,buff)
            })


        }


        return result;
    }

    async readByName(client:ModbusRTU, name:string):Promise<number>{

        const register = this.findRegisterByName(name);

        if(register === null)
          return 0;

        let len = 1;

        if(register.format == MODBUS_TYPES.FORMAT.float32 ||
            register.format == MODBUS_TYPES.FORMAT.int32 ||
            register.format == MODBUS_TYPES.FORMAT.uint32 )
            len = 2;

        const result =  await this.read(client,register.address,2);

      //  console.log(result);
        return result[0].value;

    }

    async read(client:ModbusRTU, addr:number, addrCount = 1):Promise<MODBUS_TYPES.IREADRESULT[]>{

        client.setID(this.deviceId);



 //       console.log('readHoldingRegisters',addr,addrCount);
        var buff:undefined|IBufferResult = undefined;
        let failCount = 0;
        while(failCount < 3)
        try{
           buff = await client.readHoldingRegisters(addr,addrCount);
          // console.log(buff);
           failCount = 99;
        }catch(e){
            failCount++;
        }

        if(typeof buff !== 'undefined')
            return this.processResult(addr,addrCount,buff);
        else
            return [];
    }

    static getResult(results:MODBUS_TYPES.IREADRESULT[],name:string):number{

        for(let i=0; i< results.length; i++){
            if(results[i].name === name)
              return results[i].value;
        }

        return -9999999;

    }

    async autoRead(client:ModbusRTU):Promise<MODBUS_TYPES.IREADRESULT[]>{

          client.setID(this.deviceId);

          let result:MODBUS_TYPES.IREADRESULT[] = [];

          for(let i = 0; i<this.map.requests.length; i++){
                const req = this.map.requests[i];

                const buff = await client.readHoldingRegisters(req.startAddress,req.registerCount);
           //     console.log(buff);
                result = result.concat(this.processResult(req.startAddress,req.registerCount,buff));
          }
         

          return result;
    }


    async writeSingleRegister(client:ModbusRTU, addr:number, value:number):Promise<number>{

        client.setID(this.deviceId);

        const result = await client.writeRegister(addr,value) as IWriteResult;
        return result.value
    }


    async writeSingleRegisterByName(client:ModbusRTU, name:string, value:number):Promise<number>{



        const register = this.findRegisterByName(name);

        if(register === null)
          return -1;


        return this.writeSingleRegister(client,register.address,value);
    }    

}

