export enum FORMAT {'int16','uint16','int32','uint32','float32'};


export const MAP_LOCATION=__dirname+'/../maps';

export interface IREQUEST{
    startAddress:number; registerCount:number;
}

export interface IREGISTER{
    name:string; address:number, format:FORMAT, scale:number;
}


export class IMODBUSMAP {
    public requests:IREQUEST[] = [];
    public registers:IREGISTER[] = [];
}


export interface IREADRESULT{
     name:string
    ,address:number
    ,value:number
}