export async function sleep (ms:number):Promise<void>{
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  } 



  export async function sleepUntil (condition:()=>Promise<boolean>, timeout:number):Promise<boolean>{
      const sleepInterval = 1000;
      const maxLoops = timeout/sleepInterval;
      
      for(let i=0; i< maxLoops; i++){
        if(await condition())
           return true;

        await sleep(sleepInterval);

      }

      return false;

  } 