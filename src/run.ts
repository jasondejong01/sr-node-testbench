import {Hexmod} from './hexmod'


const SERIALNUMBER_ADDRESS = 0x74c;
const FWVER_ADDRESS = 0x1d371;

const DEFAULT_SERIAL = 61003;


var args = process.argv.slice(2);

const serialnumber = args[0];


async function f(){
const HEX = new Hexmod();
await HEX.load('C:/Users/61425/Documents/Software/mplabx-workspace/solar-relay-firmware/dist/default/production/solar-relay-firmware.production.hex');


console.log('Processing firmware ver:',HEX.getIntAt(FWVER_ADDRESS));
console.log('Serial Number:',serialnumber);

console.log('Searching for default serial 61003');
console.log(HEX.findDataAddress(61003));


const defaultSerialFromHex = HEX.getIntAt(SERIALNUMBER_ADDRESS);

if(defaultSerialFromHex != DEFAULT_SERIAL){
    console.log('ERROR: The firware File might have changed, I dont think the address we have for the serial number is correct');
    process.exit();
}


 console.log('Inserting Serial number at address ',SERIALNUMBER_ADDRESS);
 HEX.setIntAt(SERIALNUMBER_ADDRESS, +serialnumber);


await HEX.writeToFile(serialnumber+'.hex');

}




f();