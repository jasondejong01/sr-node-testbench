import { ModbusDevice } from "./modbus-device";
import ModbusRTU from 'modbus-serial';
import {ENV} from './env-nowifi';
import fs from 'fs';
import { sleep, sleepUntil } from "./sleep";
import { Hexmod } from "./hexmod";
import moment from 'moment'
import * as MODBUS_TYPES from './modbus-typedefs'

var args = process.argv.slice(2);



if(args.length != 1 && args.length != 2){
    console.log('Usage: ts-node run <serialnumber> <skipprogram>');
    process.exit();
}


const SERIALNUMBER:number = +args[0];
let SKIPPROGRAM      = 0;

if(args.length == 2)
    SKIPPROGRAM = +args[1];



write(SERIALNUMBER,'***');
write(SERIALNUMBER,'***');
write(SERIALNUMBER,'***');
write(SERIALNUMBER,'*******************************************************************');
write(SERIALNUMBER,'*                                                                 *');
write(SERIALNUMBER,'*                                                                 *');
write(SERIALNUMBER,'*                  SOLAR RELAY TEST BENCH v2.0                    *');
write(SERIALNUMBER,'*                                                                 *');
write(SERIALNUMBER,'*                                                                 *');
write(SERIALNUMBER,'*******************************************************************');
write(SERIALNUMBER,'');
write(SERIALNUMBER,'Firmware File: ',ENV.firmware.inputfile);

if(!fs.existsSync(ENV.firmware.inputfile)){
    write(SERIALNUMBER,'ERROR: Firmware file not ',ENV.firmware.inputfile);
    process.exit();
}



write(SERIALNUMBER,'connecting to test bench');



const client = new ModbusRTU();
client.setTimeout (ENV.timeoutms);


        // establish the serial connection. once the connection is established a call to run()
        // will be made
        client.connectRTUBuffered(ENV.serialPort, { baudRate: ENV.baudRate
                                                ,parity:ENV.parity as 'none'|'even'|'odd'
                                                , stopBits:ENV.stopBits })                                   
                .then(async()=>{

                        write(SERIALNUMBER,'connected!');
                        await run();                                                        
                })
                .catch((error)=>{

                    console.log(error);
                    process.exit();
            

                });



function beep(){
    console.log('\x07');
}

function write(serialNo:number,...args:any[]):void{

    console.log(...args);

    let msg = moment().format('DD-MM-YYYY HH:mm')+' - ';
    for (let i= 0; i< args.length; i++)
       msg += args[i];

    fs.appendFileSync(ENV.firmware.outdir+serialNo+'.log',msg+'\r\n');
}


function error(serialNo:number,...args:any[]):void{
    write(serialNo,'ERROR',...args);
    beep();
    process.exit();
}




async function pauseme():Promise<void>{

    return new Promise((resolve)=>{


        console.log('Press ENTER to continue.');
        process.stdin.once('data', function () {
          resolve();
        });
    

    });


}


async function ipecmd(filename:string):Promise<boolean>{

    return new Promise((resolve)=>{
            const exec = require('child_process').exec;


            let c = '"C:\\Program Files\\Microchip\\MPLABX\\v5.45\\mplab_platform\\mplab_ipe\\ipecmd"';
            c += ' -P24FJ128GA306';  // device family
            c += ' -TPICD4';         // specify which programmer to use
            c += ' -E';              // Erase the device
            c += ' -M';              // program device
            c += ' -OL';             // release from reset
            c += ' -F'+'"'+filename+'"';

            write(SERIALNUMBER,c);
            exec(c,(error:any, stdout:any, stderr:any) => {

                    write(SERIALNUMBER,'stdout:',stdout);

                    if(typeof stderr !== 'undefined' && stderr !== null)
                      write(SERIALNUMBER,'stderr:',stderr);
                    
                    if(typeof stderr !== 'undefined' && stderr !== null)
                      write(SERIALNUMBER,'error :',stderr);

                    if(error === null)
                        resolve(true);
                    else
                        resolve(false);

            });

        });       
}



async function loadFirmware(serialNumber:number):Promise<void>{

       const HEX = new Hexmod();
       await HEX.load(ENV.firmware.inputfile);


       console.log(HEX.findDataAddress(61003));



       write(SERIALNUMBER,'Firmware file loaded.');
       const ver = HEX.getIntAt(ENV.firmware.version_address);
       write(SERIALNUMBER,'firmware version in file: '+ver+' expected '+ENV.firmware.version);

       if(ENV.firmware.version !== ver){
            error(SERIALNUMBER
            ,'The firmware version in the file is different to the expected firmware version'
            ,'check env.ts and make sure fwversion_address, and the firmware_ver are correct.'
            ,'If they are correct there is a problem with the firware file.'
            );
           
       }


      

       if(HEX.getIntAt(ENV.firmware.serialnumber_address) !== ENV.firmware.default_serialnumber){
           error(SERIALNUMBER,
                   'ERROR: The serial number at address',HEX.DecToHex(ENV.firmware.serialnumber_address),
                    'was ',HEX.getIntAt(ENV.firmware.serialnumber_address),' but we are expecting '+ENV.firmware.default_serialnumber
            );
       }


       write(serialNumber,'Inserting serial number ',serialNumber, ' at address '+'0x',HEX.DecToHex(ENV.firmware.serialnumber_address));

       HEX.setIntAt(ENV.firmware.serialnumber_address,serialNumber);

       const outfilename = ENV.firmware.outdir+serialNumber+'_'+ver+'.hex';
       await HEX.writeToFile(outfilename);

    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'RUNNING PROGRAMMER:');
    const programresult = await ipecmd(outfilename);
    write(SERIALNUMBER,'');

    if(!programresult){
        error(SERIALNUMBER,'ERROR: Programming Finished with error');
    }

    

}


function flt(n:number):string{

    return n.toFixed(2).padStart(8,' ');

}




function check1( label:string, meas:number,ref:number,band:number):number{

    let result = 1;

    if(meas <= ref+(ref*band) && meas >= ref-(ref*band)){
        write(SERIALNUMBER,label.padEnd(20,' ')+': PASS');
        result = 0;
     }else{
        write(SERIALNUMBER,label.padEnd(20,' ')+': FAIL','got',meas.toFixed(4),'expected',ref.toFixed(4),'+-',(ref*band).toFixed(4));
     }  


     return result;

}



function analyze(dc_bench:MODBUS_TYPES.IREADRESULT[]
                ,ac_bench:MODBUS_TYPES.IREADRESULT[]
                ,dc_dut  :MODBUS_TYPES.IREADRESULT[]
                ,ac_dut  :MODBUS_TYPES.IREADRESULT[]
                ,clocktest_result:number){

     let result = 0;


     result += check1('DUT1 DC Voltage',ModbusDevice.getResult(dc_dut,'v_r'),0,0);
     result += check1('DUT1 AC Voltage',ModbusDevice.getResult(ac_dut,'v_r'),ModbusDevice.getResult(ac_bench,'vr'),ENV.bands.v);
     result += check1('DUT1 DC Ir',ModbusDevice.getResult(dc_dut,'i_r'),0,0);
     result += check1('DUT1 AC Ir',ModbusDevice.getResult(ac_dut,'i_r'),ModbusDevice.getResult(ac_bench,'i_r'),ENV.bands.i);
     result += check1('DUT1 DC Is',ModbusDevice.getResult(dc_dut,'i_s'),0,0);
     result += check1('DUT1 AC Is',ModbusDevice.getResult(ac_dut,'i_s'),ModbusDevice.getResult(ac_bench,'i_s'),ENV.bands.i);



     if(result > 0){

         error(SERIALNUMBER,'Analysis show ERROR');

     }


}


async function readNoResponse(device:ModbusDevice, client:ModbusRTU, reg:string){

    try{await device.readByName(client,reg);}catch(e){}
}




async function run(){

    const bench = new ModbusDevice('test-bench',2);
    const dut = new ModbusDevice('solar-relay-nowifi',1);
    
    await bench.init();
    await dut.init();
    

    if(!SKIPPROGRAM){
        write(SERIALNUMBER,'loading firmware');
        await loadFirmware(+SERIALNUMBER);

        await sleep(15000);
        // program the device
    }else{
        write(SERIALNUMBER,'Waiting until the bootloader has finished');
        await pauseme();
    }

    write(SERIALNUMBER,'Retrieving Measurements from TEST BOARD');
    const dc_bench_measures = await bench.read(client,108,18);




    write(SERIALNUMBER,'MEASUREMENTS - D C- TESTBOARD:');
    write(SERIALNUMBER,'                             AC Voltage: ',flt(ModbusDevice.getResult(dc_bench_measures,'vr')||0));
    write(SERIALNUMBER,'                                CTR rms: ',flt(ModbusDevice.getResult(dc_bench_measures,'i_r')||0));
    write(SERIALNUMBER,'                                CTS rms: ',flt(ModbusDevice.getResult(dc_bench_measures,'i_s')||0));
    write(SERIALNUMBER,'                      DUT1 TRIAC Output: ',flt(ModbusDevice.getResult(dc_bench_measures,'dut1_vrms')||0));
    write(SERIALNUMBER,'                      DUT2 TRIAC Ouptut: ',flt(ModbusDevice.getResult(dc_bench_measures,'dut2_vrms')||0));
    write(SERIALNUMBER,'   5v SUpply Current No DUT Powered(mA): ',flt(ModbusDevice.getResult(dc_bench_measures,'5v_noload_i')||0));
    write(SERIALNUMBER,'   5V Supply Current Avg(mA)           : ',flt(ModbusDevice.getResult(dc_bench_measures,'5v_i_avg')||0));
    write(SERIALNUMBER,'   5V Supply Current Max(mA)           : ',flt(ModbusDevice.getResult(dc_bench_measures,'5v_i_max')||0));
    write(SERIALNUMBER,'   5V Supply Current Min(mA)           : ',flt(ModbusDevice.getResult(dc_bench_measures,'5v_i_min')||0));
    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'');

    
   
    

    write(SERIALNUMBER,'Reading measurements from DUT 1');   
    const dc_dut_measures = await dut.read(client,4,26);
  

    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'MEASURMENTS - DC - DUT1');
    write(SERIALNUMBER,'                                   Vrms:', flt(ModbusDevice.getResult(dc_dut_measures,'v_r')||0));
    write(SERIALNUMBER,'                             Imains-rms:', flt(ModbusDevice.getResult(dc_dut_measures,'i_r')||0));
    write(SERIALNUMBER,'                             Isolar-rms:', flt(ModbusDevice.getResult(dc_dut_measures,'i_s')||0));
    write(SERIALNUMBER,'                             Pmains-rms:', flt(ModbusDevice.getResult(dc_dut_measures,'pwr_r')||0));
    write(SERIALNUMBER,'                             Psolar-rms:', flt(ModbusDevice.getResult(dc_dut_measures,'pwr_s')||0));
    write(SERIALNUMBER,'                             Freq mains:', flt(ModbusDevice.getResult(dc_dut_measures,'freq_r')||0));
    write(SERIALNUMBER,'                             Freq solar:', flt(ModbusDevice.getResult(dc_dut_measures,'freq_s')||0));
    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'');



    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'Next step is to apply HV');
    write(SERIALNUMBER,'');

    await sleep(300);

    write(SERIALNUMBER,'** WARNING: HV Being applied **');

    await readNoResponse(bench,client,'dut1_hv_power');
    await sleep(300);

    if(!await sleepUntil(  async ()=>{return (await bench.readByName(client,'dut1_hv_applied')) == 1},10000)){
        error(SERIALNUMBER,'Failed to get a Apply HV power to DUT1 - Test bnech is not working');
     }


    await sleep(300);

    write(SERIALNUMBER,'HV has been applied');
    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'Wait until bootlader finishes...');
    await pauseme();



   write(SERIALNUMBER,'Turning DUT1 Output on');
   await dut.writeSingleRegisterByName(client,'duty',1);

   await sleep(3000);


    write(SERIALNUMBER,'Reading measurements from DUT 1');   
    const ac_dut_measures = await dut.read(client,4,26);

    await sleep(300);

    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'MEASUREMENTS - AC -DUT1');
    write(SERIALNUMBER,'                                   Vrms:', flt(ModbusDevice.getResult(ac_dut_measures,'v_r')||0));
    write(SERIALNUMBER,'                             Imains-rms:', flt(ModbusDevice.getResult(ac_dut_measures,'i_r')||0));
    write(SERIALNUMBER,'                             Isolar-rms:', flt(ModbusDevice.getResult(ac_dut_measures,'i_s')||0));
    write(SERIALNUMBER,'                             Pmains-rms:', flt(ModbusDevice.getResult(ac_dut_measures,'pwr_r')||0));
    write(SERIALNUMBER,'                             Psolar-rms:', flt(ModbusDevice.getResult(ac_dut_measures,'pwr_s')||0));
    write(SERIALNUMBER,'                             Freq mains:', flt(ModbusDevice.getResult(ac_dut_measures,'freq_r')||0));
    write(SERIALNUMBER,'                             Freq solar:', flt(ModbusDevice.getResult(ac_dut_measures,'freq_s')||0));
    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'');




    console.log('Retrieving Measurements from TEST BOARD');
    const ac_bench_measures = await bench.read(client,108,18);

    await sleep(300);


    write(SERIALNUMBER,'MEASUREMENTS - AC - TESTBOARD:');
    write(SERIALNUMBER,'                             AC Voltage: ',flt(ModbusDevice.getResult(ac_bench_measures,'vr')||0));
    write(SERIALNUMBER,'                                CTR rms: ',flt(ModbusDevice.getResult(ac_bench_measures,'i_r')||0));
    write(SERIALNUMBER,'                                CTS rms: ',flt(ModbusDevice.getResult(ac_bench_measures,'i_s')||0));
    write(SERIALNUMBER,'                      DUT1 TRIAC Output: ',flt(ModbusDevice.getResult(ac_bench_measures,'dut1_vrms')||0));
    write(SERIALNUMBER,'                      DUT2 TRIAC Ouptut: ',flt(ModbusDevice.getResult(ac_bench_measures,'dut2_vrms')||0));
    write(SERIALNUMBER,'   5v SUpply Current No DUT Powered(mA): ',flt(ModbusDevice.getResult(ac_bench_measures,'5v_noload_i')||0));
    write(SERIALNUMBER,'   5V Supply Current Avg(mA)           : ',flt(ModbusDevice.getResult(ac_bench_measures,'5v_i_avg')||0));
    write(SERIALNUMBER,'   5V Supply Current Max(mA)           : ',flt(ModbusDevice.getResult(ac_bench_measures,'5v_i_max')||0));
    write(SERIALNUMBER,'   5V Supply Current Min(mA)           : ',flt(ModbusDevice.getResult(ac_bench_measures,'5v_i_min')||0));
    write(SERIALNUMBER,'');
    write(SERIALNUMBER,'');

   await sleep(300);

   let dutOutputVoltage = ModbusDevice.getResult(ac_bench_measures,'dut1_vrms')||0;

   if(dutOutputVoltage < 200){
        error(SERIALNUMBER,'DUT Output did not turn contactor on. Measured voltage was only',dutOutputVoltage);
   }

   write(SERIALNUMBER,'Shutting down Output of DUT1');

   await sleep(300);
   await dut.writeSingleRegisterByName(client,'duty',0);

   await sleep(3000); 
   dutOutputVoltage = await bench.readByName(client,'dut1_vrms');

   if(dutOutputVoltage > 10){
       error(SERIALNUMBER,'DUT Output did not turn contactor off. Measured voltage is still',dutOutputVoltage);
   }

   await sleep(300);

   write(SERIALNUMBER,'');
   write(SERIALNUMBER,'Turning Clock Test On DUT');

   await dut.writeSingleRegisterByName(client,'clocktest',1);

   await sleep(300);

   write(SERIALNUMBER,'Turning Clock Test On TEST BENCH');

   await bench.readByName(client,'start_clocktest');

   write(SERIALNUMBER,'waiting for clock test to finish');
   await sleep(20000);


   if(!await sleepUntil(  async ()=>{return (await bench.readByName(client,'clocktest_done')) > 0},1000)){
        error(SERIALNUMBER,'Failed to get a clock test result from the TEST BENCH');
    }

    await sleep(300);


    const clocktest_result = await bench.readByName(client,'clocktest_done');


    analyze(dc_bench_measures,ac_bench_measures,dc_dut_measures,ac_dut_measures,clocktest_result);


    write(SERIALNUMBER,"*** TESTING COMPLETE ***");

    

    process.exit();


} 

