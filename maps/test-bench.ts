import * as MODBUS_TYPES from '../src/modbus-typedefs'


export default class TESTBENCH implements MODBUS_TYPES.IMODBUSMAP{

    requests = [];

    registers = [

         {name:'dut1_dc_power'     ,address:100  ,format:MODBUS_TYPES.FORMAT.int16 ,scale:1}
        ,{name:'dut1_dc_applied'   ,address:102  ,format:MODBUS_TYPES.FORMAT.int16 ,scale:1}

        ,{name:'vr'                ,address:108  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

           // these are swapped around compared to the source code, there must be a problem 
           // with the circuit
        ,{name:'dut1_vrms'         ,address:112  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'dut2_vrms'         ,address:110  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'i_r'               ,address:114  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'i_s'               ,address:116  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'5v_noload_i'       ,address:118  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'5v_i_avg'          ,address:120  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'5v_i_max'          ,address:122  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'5v_i_min'          ,address:124  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}


        ,{name:'dut1_hv_power'     ,address:130  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'dut1_hv_applied'   ,address:132  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'start_clocktest'     ,address:140  ,format:MODBUS_TYPES.FORMAT.int16 ,scale:1}
        ,{name:'clocktest_done'      ,address:141  ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

    ];


}
