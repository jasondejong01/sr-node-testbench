import * as MODBUS_TYPES from '../src/modbus-typedefs'


export default class CATCHPOWER implements MODBUS_TYPES.IMODBUSMAP{

    requests = [
         {startAddress:4, registerCount:72}
    
    ];

    registers = [

         {name:'v_r'       ,address:4   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'i_r'       ,address:6   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'i_s'       ,address:8   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'va_r'      ,address:12   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'va_s'      ,address:14   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'pwr_r'     ,address:18   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'pwr_s'     ,address:20   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'freq_r'    ,address:24   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'freq_s'    ,address:26   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'pf_r'      ,address:30   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'pf_s'      ,address:32   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'whe_r'     ,address:36   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'whe_s'     ,address:38   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'whi_r'     ,address:42   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'whi_s'     ,address:44   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'quad_r'    ,address:48   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'quad_s'    ,address:50   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}

        ,{name:'wh_int_r'  ,address:54   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'wh_int_s'  ,address:56   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}        

        ,{name:'varh_neg_r'  ,address:60   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'varh_neg_s'  ,address:62   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}        
    
        ,{name:'varh_pos_r'  ,address:66   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'varh_pos_s'  ,address:68   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1} 
        
        ,{name:'var_r'       ,address:72   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1}
        ,{name:'var_s'       ,address:74   ,format:MODBUS_TYPES.FORMAT.float32 ,scale:1} 
        
        ,{name:'ct_ratio'    ,address:106   ,format:MODBUS_TYPES.FORMAT.int16 ,scale:1} 

        ,{name:'duty'         ,address:216   ,format:MODBUS_TYPES.FORMAT.int16 ,scale:1} 
        ,{name:'duty_release' ,address:217   ,format:MODBUS_TYPES.FORMAT.int16 ,scale:1} 
        
        ,{name:'clocktest'     ,address:220   ,format:MODBUS_TYPES.FORMAT.int16 ,scale:1} 

        ,{name:'wifi-on'     ,address:2000   ,format:MODBUS_TYPES.FORMAT.int16 ,scale:1} 

    ];


}
